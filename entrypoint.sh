#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/default.d/default.conf
nginx -g 'deamon off;'
